(window => {
    window.dragonFlyEvent = {
        populate: populate
    };

    function populate() {
        let evtData = JSON.parse(window.sessionStorage.getItem('currentEventData'));

        window.dragonFlyAPI.getMedia(evtData.id, evtData.images[0].id)
                           .then(response => [window.btoa(response.text()), response.headers])
                           .then(args => `data:image/jpeg;base64,${args[0]}`)
                           .then(dataURI => generate$Event(evtData, dataURI))
                           .then(() => generate$Status(evtData))
                           .catch(e => {
                               generate$Event(evtData);
                               window.dragonFlyNotifications.error(`${e}`);
                           });
    }

    function generate$Event(evtData, dataURI) {
        let $eventFragment  = document.createDocumentFragment(),
            $title          = document.createElement('div'),
            $mediaContainer = document.createElement('div'),
            $img            = document.createElement('img'),
            $comments       = document.createElement('ul'),
            commentFragment = create$Comments(evtData.comments);

        $title.textContent = evtData.name;
        $img.src = dataURI || '';
        $mediaContainer.appendChild($img);
        $comments.appendChild(commentFragment);

        [$title, $mediaContainer, $comments].forEach($elm => $eventFragment.appendChild($elm));

        document.getElementById('event').appendChild($eventFragment);
    }

    function create$Comments(commentData) {
        let $commentsFragment = document.createDocumentFragment(),
            $commentMap       = commentData.map(comment => {
                let $comment = document.createElement('li'),
                    $commentFrom = document.createElement('div'),
                    $commentText = document.createElement('p');

                $commentFrom.textContent = comment.from;
                $commentText.textContent = comment.text;

                [$commentFrom, $commentText].forEach($elm => $comment.appendChild($elm));

                return $comment;
            });

        $commentMap.forEach($elm => $commentsFragment.append($elm));

        return $commentsFragment;
    }

    function generate$Status(evtData) {
        window.dragonFlyAPI.getStatus(evtData.id)
                           .then(response => build$Statuses(evtData, response))
                           .catch(e => {
                               window.dragonFlyNotifications.error(`${e}`);
                               build$Statuses(evtData);
                           });
    }

    function build$Statuses(evtData, response) {
        if (response && response.status === 204) {
            window.dragonFlyNotifications.valid(`You haven't set a status yet`);
            document.getElementById('status-options').appendChild(build$Status(evtData.id, response));

        } else if (response && response.status !== 200) {
            window.dragonFlyNotifications.error(`issue ${response.status} ${response.text()}`);
            document.getElementById('status-options').appendChild(build$Status(evtData.id, response));

        } else {
            response.json().then(parsed => document.getElementById('status-options').appendChild(build$Status(evtData.id, parsed)));
        }
    }

    function build$Status(evtId, response) {
        let $statusBlock = document.createElement('div'),
            $label       = document.createElement('label'),
            $input       = document.createElement('input');

        $statusBlock.classList.add('status-block');

        $label.setAttribute('for', 'going-or-not');
        $label.textContent = 'Going';

        $input.id = 'going-or-not';
        $input.setAttribute('type', 'checkbox');


        if (typeof response === 'object' && response.body.coming) {
            $input.setAttribute('checked', true);
        }

        $input.addEventListener('click', evt => {
            evt.preventDefault();
            evt.stopPropagation();

            window.dragonFlyAPI.saveStatus(evtId, {body: {coming: $input.hasAttribute('checked') ? false : true}})
                               .then(response => refresh(response))
                               .catch(e => window.dragonFlyNotifications.error(`${e}`));
        });

        [$label, $input].forEach($elm => $statusBlock.append($elm));

        return $statusBlock;
    }

    function refresh(response) {
        [document.getElementById('event'),
         document.getElementById('status-options')].forEach($elm => {
             while ($elm.firstChild) {
                 $elm.removeChild($elm.firstChild);
             }
         });

        window.dragonFlyEvent.populate();
    }

})(this);
