(window => {

    var DragonFlyEventList = function() {};

    DragonFlyEventList.prototype.populate = function() {

        window.networker.showLoader();

        return window.dragonFlyAPI.getEvents()
                                  .then(eventData => this.createEvents(eventData))
                                  .then($eventNodesFragment => {
                                      window.networker.showLoader(false);
                                      document.getElementById('event-list').appendChild($eventNodesFragment);
                                  })
                                  .catch(e => window.dragonFlyNotifications.error(`${e}`));
    };

    DragonFlyEventList.prototype.createEvents = function(eventData) {
        let $eventListFragment = document.createDocumentFragment(),
            eventMap           = eventData.map(evt => generateList$Event(evt));

        eventMap.forEach($elm => $eventListFragment.appendChild($elm));

        return $eventListFragment;
    }

    function generateList$Event(evtData) {
        let $event            = document.createElement('li'),
            $thumb            = document.createElement('img'),
            $eventName        = document.createElement('div'),
            $eventDescription = document.createElement('div'),
            $eventLocation    = document.createElement('div');


        $event.classList.add('event-item');
        $event.addEventListener('click', evt => {
            evt.preventDefault();
            evt.stopPropagation();

            // store everything in session, would have used the api if the individual event item were available
            setCurrentEvent(evtData);
            window.location.assign(`${window.location.origin}/#/event`);
        });

        $thumb.classList.add('event-thumb');
        $thumb.src = '';

        $eventName.classList.add('event-name');
        $eventName.textContent = evtData.name;

        $eventDescription.classList.add('event-description');
        $eventDescription.textContent = evtData.description;

        $eventLocation.classList.add('event-location');
        $eventLocation.textContent = `${evtData.location.name} \b ${evtData.location.address} \b ${evtData.location.city}, ${evtData.location.state}`;

        [$thumb, $eventName, $eventDescription, $eventLocation].forEach($elm => $event.appendChild($elm));

        return $event;
    }

    function setCurrentEvent(evtData) {
        window.sessionStorage.setItem('currentEventData', JSON.stringify(evtData));
    }

    window.dragonFlyEventList = new DragonFlyEventList();

})(this);
