(window => {
    'use strict';

    var Networker = function(opts) {
        this.creds     = {
            username: 'joshua',
            password: 'evalpass'
        };

        this.basePath = (opts && opts.basePath) || `http://dev.dragonflyathletics.com:1337/api/dfkey`;
        this.auth     = 'Basic ' + window.btoa(`${this.creds.username}:${this.creds.password}`);

    }

    //PRIMARY
    Networker.prototype.request = function(options) {
        return window.fetch(options.url,
                            Object.assign({}, {
                                method: options.method,
                                headers: this.setHeaders(options.headers, options.useAuth || true),
                                body: options.isForm ? options.body : window.JSON.stringify(options.body)}))
                      .then(response => options.sendBackResponse ? response : response.json());
    };

    Networker.prototype.setHeaders = function(headers, useAuth) {
        return useAuth ? Object.assign({Authorization: this.auth}, headers || {}) : headers || {};
    };

    Networker.prototype.showLoader = function(negate) {
        let $loader = document.getElementById('loading');
        $loader.classList[(negate === false ? 'add' : 'remove')]('hide');
    };

    window.networker = new Networker();

})(this);
