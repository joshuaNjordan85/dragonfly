(window => {
    'use strict';

    var API = function() {
        if (!window.networker) {
            throw new Error('networker must exist on window to use Upload module');
        }

        this.networker = window.networker;
        this.base      = window.networker.basePath;
    };

    API.prototype.getEvents = function() {
        let getEventsRequestOpts = {
            method: 'GET',
            url: `${this.base}/events`
        };

        return this.networker.request(getEventsRequestOpts);
    };

    API.prototype.getMedia = function(eventId, mediaId) {
        let getMediaRequestOpts = {
            method: 'GET',
            url: `${this.base}/events/${eventId}/media/${mediaId}`,
            sendBackResponse: true
        };

        return this.networker.request(getMediaRequestOpts);
    };

    API.prototype.getStatus = function(eventId) {
        let getStatusRequestOpts = {
            method: 'GET',
            url: `${this.base}/events/${eventId}/status/${window.networker.creds.username}`,
            sendBackResponse: true
        };

        return this.networker.request(getStatusRequestOpts);
    };

    API.prototype.saveStatus = function(eventId, body) {
        let saveStatusRequestOpts = {
            method: 'PUT',
            url: `${this.base}/events/${eventId}/status/${window.networker.creds.username}`,
            body: body,
            headers: {
                'Content-Type': 'application/json'
            },
            sendBackResponse: true
        };

        return this.networker.request(saveStatusRequestOpts);
    };

    window.dragonFlyAPI = new API();

})(this);
