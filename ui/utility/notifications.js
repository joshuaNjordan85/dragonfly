(window => {

    var DragonFlyNotifications = function($notification) {
        this.$notification = document.getElementById($notification || 'notification');
    };

    DragonFlyNotifications.prototype.valid = function($text) {
        this.$notification.classList.remove('error');
        this.$notification.classList.add('valid');
        this.$notification.textContent = $text;
        this.hide();
    };

    DragonFlyNotifications.prototype.error = function($text) {
        this.$notification.classList.remove('valid');
        this.$notification.classList.add('error');
        this.$notification.textContent = $text;
        this.hide(2000);
    };

    DragonFlyNotifications.prototype.hide = function(time) {
        window.setTimeout(() => {
            this.$notification.classList.remove('error');
            this.$notification.classList.remove('valid');
            this.$notification.classList.add('hide');
        }, (time || 1500));
    }

    window.dragonFlyNotifications = new DragonFlyNotifications();

})(this);
