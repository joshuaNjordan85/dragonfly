(window => {
    'use strict';

    var formatHash        = pageHash => `#/${pageHash}`,
        handleExtraEvents = () => {
            if (window.location.hash.search(/^#\/eventList/g) !== -1) {
                //If we are not looking at an event anymore, then it's safe to kill the session event id before repopulating everything
                window.sessionStorage.clear();
                window.dragonFlyEventList.populate();

            } else if (window.location.hash.search(/^#\/event/g) !== -1) {
                window.dragonFlyEvent.populate();
            }
        },
        loadInHTMLContent = () => {
            let currentHash    = window.location.hash.slice(2),
                dynContentPath = `ui/views/${currentHash}/${currentHash}.html`;

            window.fetch(dynContentPath)
                  .then(response => response.text())
                  .then(html => document.getElementById('dynamic-content').innerHTML = html)
                  .then(() => handleExtraEvents())
                  .catch(e => console.error(e));
        },
        ViewLoader = function(pageHashes) {
            if (pageHashes && !(Array.isArray(pageHashes) && pageHashes.length > 0)) {
                throw new Error('pathHashes must be of type array to use ViewLoader');
            }

            // will always get here and if pathHashes doesn't exist, it will evaluate to false
            this.pageHashes = pageHashes.map(pageHash => formatHash(pageHash)) || [];
        },
        initialize = pageHashes => {
            let vLoader = new ViewLoader(pageHashes),
                pHs     = vLoader.pageHashes;

            window.addEventListener('load', () => {
                if ('onhashchange' in window) { // manage back and forward browser buttons
                    window.onhashchange = () => loadInHTMLContent();
                }
            });

            // for adding in data on refresh
            return {
                loadInHTMLContent: loadInHTMLContent
            };
        };

    window.viewLoader = initialize(['eventList', 'event']);

})(this);
