(window => {
    'use strict';

    window.onload = () => setUp();

    function setUp() {
        window.location.assign(`${window.location.origin}/#/eventList`);
        window.viewLoader.loadInHTMLContent();

        window.onclose = () => {
            window.onhashchange = null;
            window.sessionStorage.clear();
        }
    }

})(this);
